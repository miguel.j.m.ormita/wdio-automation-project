import {assert} from 'chai';
import {basicUser} from '../data/user-data';
import NavigationFixture from '../fixtures/navigation';
import {TestConstants} from '../data/constants';
import RegistrationFixture from '../fixtures/registration-fixtures';

describe('This feature test', function(){

    let testUserInfo = basicUser;

    it('should access the Sign On page', function(){
        NavigationFixture.accessMTSingOnPage(TestConstants.mtUrl);

    });

    it('should access the Registration page', function(){

        //Click on the Register button
        NavigationFixture.accessRegistrationPage();
    });

    it('should enter the user information', function(){

        //Click on the Register button
        RegistrationFixture.enterRegistrationUserInfo(testUserInfo);

    });

    it('should display the "Thank you" page', function(){

        //Click Submit
        RegistrationFixture.submitRegistration();
    });

});