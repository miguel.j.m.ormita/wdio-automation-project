import SignOnPage from '../pages/sign-on-page';
import MTRegistrationPage from '../pages/registration-page';

class NavigationFixture{

    accessMTSingOnPage(url){
        browser.url(url);

        new SignOnPage();
    }

    accessRegistrationPage(){

        let singONPage = new SignOnPage();
        singONPage.clickRegister();

    }
}

export default new NavigationFixture();