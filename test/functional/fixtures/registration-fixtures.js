import RegistrationPage from '../pages/registration-page';

class RegistrationFixture{

    enterRegistrationUserInfo(userInfo){
        
        let registrationPage = new RegistrationPage();

        //Enter first name
        registrationPage.enterFirstName(userInfo.firstName);

        //Enter last name
        registrationPage.enterLastName(userInfo.lastName);

        //Enter user Name
        registrationPage.enterUserNameEmail(userInfo.userNameEmail);

        //Enter password
        registrationPage.enterPassword(userInfo.password);

        //Enter confirm Password
        registrationPage.enterConfirmPassword(userInfo.password);
    }

    submitRegistration(){

        let registrationPage = new RegistrationPage();
        registrationPage.clickSubmit();
    }
}

export default new RegistrationFixture();