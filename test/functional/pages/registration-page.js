import RegistrationThankPage from '../pages/registration-thank-page';


 class RegistrationPage{

    constructor(){
        try{
            this._init();
        }catch(err){
            throw new Error('Page not loaded! The '+this._pageName+' is not displayed. '+err);
        }
    }

    get _pageName(){
        return  'Mercury Tours Registration page';
    }

     get _imgRegister(){
         return browser.element('//img[contains(@src,"register")]');
     }

     get _inputFirstName(){
         return browser.element('//input[@name="firstName"]');
     }

     get _inputLastName(){
         return browser.element('//input[@name="lastName"]');
     }

     get _inputUsernameEmail(){
         return browser.element('//input[@name="email"]');
     }

     get _inputPassword(){
         return browser.element('//input[@name="password"]');
     }

     get _inputConfirmPassword(){
         return browser.element('//input[@name="confirmPassword"]');
     }

     get _buttonSubmitRegister(){
         return browser.element('//input[@name="register"]');
     }

     enterFirstName(firstName){
        this._inputFirstName.waitForVisible(6000);
        this._inputFirstName.setValue(firstName);
     }


     enterLastName(lastName){
         this._inputLastName.waitForVisible(6000);
         this._inputLastName.setValue(lastName);
     }

     enterUserNameEmail(userNameEmail){
         this._inputUsernameEmail.waitForVisible(6000);
         this._inputUsernameEmail.setValue(userNameEmail);
     }

     enterPassword(password){
         this._inputPassword.waitForVisible(6000);
         this._inputPassword.setValue(password);
     }

     enterConfirmPassword(confirmPassword){
         this._inputConfirmPassword.waitForVisible(6000);
         this._inputConfirmPassword.setValue(confirmPassword);
     }

     clickSubmit(){
        this._buttonSubmitRegister.waitForVisible(6000);
        this._buttonSubmitRegister.click();

        return new RegistrationThankPage();
     }

     _init(){
        this._imgRegister.waitForVisible(30000);
    }
 }

 export default RegistrationPage;