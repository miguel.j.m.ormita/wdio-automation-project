
 class RegistrationThankPage{

    constructor(){
        try{
            this._init();
        }catch(err){
            throw new Error('Page not loaded! The '+this._pageName+' is not displayed. '+err);
        }
    }

    get _pageName(){
        return  'Mercury Tours Registration Thank You page';
    }

     get _textThankYou(){
         return browser.element('//p//*[contains(text(),"Thank you")]');
     }

     _init(){
        this._textThankYou.waitForVisible(30000);
    }


 }

 export default RegistrationThankPage;