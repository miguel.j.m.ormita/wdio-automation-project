import RegistrationPage from './registration-page';

 class SignOnPage{

    constructor(){
        try{
            this._init();
        }catch(err){
            throw new Error('Page not loaded! The '+this._pageName+' is not displayed. '+err);
        }
    }

    get _pageName(){
        return  'Mercury Tours Home page';
    }

    get _inputSingIn(){
        return browser.element('//input[@name="login"]');
    }

    get _buttonLinkRegister(){
        return browser.element('//a[text()="REGISTER"]');
    }

    _init(){
        this._inputSingIn.waitForVisible(30000);
    }

    clickRegister(){
        this._buttonLinkRegister.waitForVisible(6000);
        this._buttonLinkRegister.click();

        return new RegistrationPage();
    }


 }

 export default SignOnPage;